git filter-branch --env-filter '
if test "$GIT_AUTHOR_EMAIL" = "Xu.Liu@ygomi.com"
then
    GIT_AUTHOR_NAME="Xu Liu"
    GIT_AUTHOR_EMAIL="Xu.Liu@ygomi.com"
fi
export GIT_AUTHOR_NAME
export GIT_AUTHOR_EMAIL
'
