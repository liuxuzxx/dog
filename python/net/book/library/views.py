from django.shortcuts import render
from django.http import HttpResponse
# Create your views here.

def welcome(request):
    return HttpResponse('Welcome to library of China!')

def information(request):
    university = request.GET['university']
    return HttpResponse('University is :'.join(university))
