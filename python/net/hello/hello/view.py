from django.http import HttpResponse

def view(request):
	information = 'Path:'.join(request.path).join("\nMethod:").join(request.method)
	print(information)
	return HttpResponse(information)
