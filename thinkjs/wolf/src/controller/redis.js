const Base = require('./base.js');

module.exports = class extends Base{
    configAction(){
        const redis = this.config('redis');
		this.body = redis;
    }
}
