// default config
module.exports = {
  workers: 4,
  port: 12345,
  redis:{
    host: '127.0.0.1',
	port: 6379
  }
};
